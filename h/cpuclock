/*
 * Copyright (c) 2016, RISC OS Open Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of RISC OS Open Ltd nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef CPUCLOCK_H
#define CPUCLOCK_H

#include "kernel.h"

#include "Interface/CPUClkDevice.h"

typedef struct
{
  uint32_t reloc[2]; /* Relocation offsets are located directly before the device, so assembler code can easily find them */
  struct cpuclkdevice_0_2 dev;
} myclkdev_t;

extern myclkdev_t myclkdev;

extern void cpuclock_init(void);
extern _kernel_oserror *cpuclock_shutdown(void);

/* Assembler veneers */
extern bool cpuclock_asm_Activate(struct device *);
extern void cpuclock_asm_Deactivate(struct device *);
extern void cpuclock_asm_Reset(struct device *);
extern int32_t cpuclock_asm_Sleep(struct device *, int32_t state);
extern uint32_t cpuclock_asm_NumSpeeds(struct cpuclkdevice *cpuclk);
extern uint32_t cpuclock_asm_Info(struct cpuclkdevice *cpuclk, uint32_t idx);
extern uint32_t cpuclock_asm_Get(struct cpuclkdevice *cpuclk);
extern int cpuclock_asm_Set(struct cpuclkdevice *cpuclk, uint32_t idx);
extern unsigned int cpuclock_asm_GetDieTemperature(struct cpuclkdevice *cpuclk);

/* C implementations */
extern bool cpuclock_Activate(struct device *);
extern void cpuclock_Deactivate(struct device *);
extern void cpuclock_Reset(struct device *);
extern int32_t cpuclock_Sleep(struct device *, int32_t state);
extern uint32_t cpuclock_NumSpeeds(struct cpuclkdevice *cpuclk);
extern uint32_t cpuclock_Info(struct cpuclkdevice *cpuclk, uint32_t idx);
extern uint32_t cpuclock_Get(struct cpuclkdevice *cpuclk);
extern int cpuclock_Set(struct cpuclkdevice *cpuclk, uint32_t idx);
extern unsigned int cpuclock_GetDieTemperature(struct cpuclkdevice *cpuclk);

#endif
